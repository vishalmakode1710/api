﻿using Microsoft.EntityFrameworkCore;
using ProductAPI.Context;
using ProductAPI.Model;

namespace ProductAPI.Repository
{
    public class ProductRepository : IProductRepository
    {
        ProductDbContext _productDbContext;
        public ProductRepository(ProductDbContext productDbContext)
        {
            _productDbContext = productDbContext;
        }
        public int AddProduct(Product product)
        {
            _productDbContext.productList.Add(product);
            return _productDbContext.SaveChanges();
        }

        public int DeleteProduct(Product productExist)
        {
            _productDbContext.Remove(productExist);
            return _productDbContext.SaveChanges();
        }

        public int EditProduct(Product product)
        {
            _productDbContext.Entry(product).State = EntityState.Modified;
            return _productDbContext.SaveChanges();
        }

        public List<Product> GetAllProducts()
        {
            return _productDbContext.productList.ToList();
        }

        public Product GetProductById(int id)
        {
            return _productDbContext.productList.Where(u => u.Id == id).FirstOrDefault();
        }

        public Product GetProductByName(string? name)
        {
            return _productDbContext.productList.Where(u => u.Name == name).FirstOrDefault();
        }
    }
}
