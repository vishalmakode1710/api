﻿using ProductAPI.Model;

namespace ProductAPI.Service
{
    public interface IProductService
    {
        bool AddProduct(Product product);
        List<Product> GetAllProducts();
        bool DeleteProduct(int id);
        Product GetProductById(int id);
        bool EditProduct(int id, Product product);
    }
}
